<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ExpenseUser extends Model
{
    protected $table = 'expense_users';

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected $fillable = [
        'user_id', 'title','amount','attachment','date','status'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopeUserPermission($query)
    {
        if(auth()->user()->type !== "manager"){
            return $query->where('user_id', auth()->id());
        }
    }

}
