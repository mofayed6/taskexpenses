<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExpenseUserRequest;
use App\Models\ExpenseUser;
use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ExpenseUsersController extends Controller
{
    /**
     * get custom value from relation
     *
     * @var string[]
     */
    protected $relations = ['user:id,name'];

    /**
     * validation use all function by auth user
     * ExpenseUsersController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * use locale scope userPermission
     * @return View
     */
    public function index()
    {
        $expenseUsers = ExpenseUser::with($this->relations)->userPermission()->get();
        return view('expense_users.index', compact('expenseUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        $expenseUsers = ExpenseUser::with($this->relations);
        Gate::authorize('expense-create', $expenseUsers);
        return view('expense_users.create');
    }

    /**
     * Create a new User Expenses OR Update Expenses When have id.
     *
     * @param ExpenseUserRequest $request
     * @return RedirectResponse
     */
    public function store(ExpenseUserRequest $request)
    {
        $userExpense = new ExpenseUser();

        if ($files = $request->file('attachment')) {
            $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $request->attachment->move(public_path('uploads'), $fileName);
            $userExpense->attachment    = $fileName;
        }

        $userExpense->title        = $request->title;
        $userExpense->user_id       = auth()->user()->id;
        $userExpense->amount        = $request->amount;
        $userExpense->date          = $request->date;
        $userExpense->status        = 0;

        $userExpense->save();

        if ($userExpense) {
            return redirect()->back()->with('success', "add Expenses");
        }
    }

    /**
     *
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|Response|View
     */
    public function show($id)
    {
        $expense_user = ExpenseUser::with($this->relations)->find($id);
        return view('expense_users.show', compact('expense_user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|Response|View
     */
    public function edit($id)
    {
        $expends = Expenses::pluck('name','id');
        $expense_user = ExpenseUser::find($id);
        $option = ['0'=>'in progress','1'=>'approved','2'=>'rejected','3'=>'cancel'];
        if(auth()->user()->type === "employee"){
            unset($option[0]);
            unset($option[1]);
            unset($option[2]);
        }else{
            unset($option[3]);
        }
        return view('expense_users.edit', compact('expends','expense_user','option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ExpenseUserRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(ExpenseUserRequest $request, $id)
    {
        $expense_user = ExpenseUser::find($id);

        if ($files = $request->file('attachment')) {
            unlink(public_path('uploads/'.$expense_user->attachment));
            $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $request->attachment->move(public_path('uploads'), $fileName);
            $expense_user->attachment    = $fileName;
        }

        $expense_user->title         = $request->title;
        $expense_user->user_id       = auth()->id();
        $expense_user->amount        = $request->amount;
        $expense_user->date          = $request->date;
        $expense_user->status        = $request->status;

        $expense_user->save();
        if ($expense_user) {
            return redirect()->back()->with('success', "updated Expenses");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $expense_user = ExpenseUser::find($id);
        if(($expense_user->attachment !== Null) && is_file(public_path($expense_user->attachment))) {
            unlink(public_path('uploads/'.$expense_user->attachment));
        }
        $expense_user->delete();

        return redirect()->back()->with('success', "deleted Expenses");
    }

    /**
     * return file download
     *
     * @param $file_name
     * @return BinaryFileResponse
     */
    public function download($file_name)
    {
        $file_path = public_path('uploads/'.$file_name);
        return response()->download($file_path);
    }
}
