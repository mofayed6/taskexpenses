<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ExpenseUser;
use Illuminate\Http\Request;

class ExpenseUsersAPIController extends Controller
{
    protected $relations = ['user:id,name'];

    /**
     * Display a listing of the ExpenseUser.
     *
     * @return array
     */
    public function index(): array
    {
        //TODO: this user to get data by auth user
        $expenseUsers = ExpenseUser::with($this->relations);

        return [
            'status'  => 200,
            'success' => true,
            'data'    => $expenseUsers->get(),
        ];
    }

    /**
     * Display the specified ExpenseUser.
     *
     * @param int $id
     * @return array
     */
    public function show($id): array
    {
        return [
            'status'  => 200,
            'success' => true,
            'data'    => ExpenseUser::with($this->relations)->findOrFail($id),
        ];
    }

    /**
     * Create a new ExpenseUser.
     *
     * @param Request $request
     * @param null $id
     * @return array
     * @throws ValidationException
     */
    public function store(Request $request, $id = null): array
    {
        $this->validate($request, [
            'title'            => 'required',
            'amount'            => 'required|int|numeric',
            'date'              => 'required|date',
        ]);

        if($id) {
            $userExpense = ExpenseUser::findOrFail($id);
            $userExpense->status        = $request->status;
        } else {
            $userExpense = new ExpenseUser;
            $userExpense->status        = 0;
        }

        $userExpense->user_id       = 1;
        $userExpense->title         = $request->title;
        $userExpense->amount        = $request->amount;
        $userExpense->date          = $request->date;

        $userExpense->save();

        return [
            'status'     => 200,
            'success'    => true,
            'data'       => $userExpense,
        ];
    }

    /**
     * Remove the specified ExpenseUser .
     *
     * @param int $id
     * @return array
     */
    public function destroy($id): array
    {
        ExpenseUser::destroy($id);

        return [
            'status'  => 200,
            'success' => true,
        ];
    }
}
