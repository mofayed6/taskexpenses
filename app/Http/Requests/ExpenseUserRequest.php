<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExpenseUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules['title']        = 'required';
        $rules['amount']        = 'required|numeric';
        $rules['date']          = 'required|date';
        $rules['attachment']    = 'mimes:pdf,xlx,csv,png,jpeg|max:2048';

        return $rules;
    }
}
