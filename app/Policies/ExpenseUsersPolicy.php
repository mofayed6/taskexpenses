<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ExpenseUsersPolicy
{
    use HandlesAuthorization;

    public function Create(User $user)
    {
        return $user->type === "manager"
            ? Response::deny('You do not own this action.')
            : Response::allow();
    }
}
