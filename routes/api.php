<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Expense User routes
Route::group(['prefix' => 'expense-users'], function () {
    Route::get('/', 'API\ExpenseUsersAPIController@index');
    Route::post('/', 'API\ExpenseUsersAPIController@store');
    Route::get('/{id}', 'API\ExpenseUsersAPIController@show');
    Route::put('/{id}', 'API\ExpenseUsersAPIController@store');
    Route::delete('/{id}', 'API\ExpenseUsersAPIController@destroy');
});
