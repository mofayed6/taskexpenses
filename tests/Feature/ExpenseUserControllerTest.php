<?php

namespace Tests\Feature;

use App\Models\Expenses;
use App\Models\ExpenseUser;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\Response;

class ExpenseUserControllerTest extends TestCase
{
    public function testIndexReturnsDataInValidFormat()
    {
        $this->json('get', 'api/expense-users')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    "status",
                    "success",
                    'data' => [
                        '*' => [
                            "id",
                            "user_id",
                            "title",
                            "amount",
                            "attachment",
                            "date",
                            "status",
                            "created_at",
                            "updated_at",
                        ]
                    ]
                ]
            );
    }


    public function testStoreWithMissingData()
    {
        $payload = [
            'user_id' => '1'
        ];
        $this->json('post', 'api/expense-users', $payload)
            ->assertStatus(422);
    }

    public function testDestroyExpenseUser()
    {

        $user = User::create([
            'name' => 'employee-test_destroy',
            'email' => 'employee-test_destroy@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123456'), // password
            'type'=>'employee',
        ]);

        $expensesUser = ExpenseUser::create(
            [
                'user_id'     => $user->id,
                'title'       => 'test2',
                'amount'      => 100,
                'date'        => "2021-03-10",
                'attachment'  => null,
                'status'      => 0
            ]
        );
        $this->json('delete', "api/expense-users/$expensesUser->id")
            ->assertStatus(200);
    }

    public function testExpenseUsersIsUpdatedSuccessfully()
    {
        $userExpenses= ExpenseUser::create([
            'user_id'     => 1,
            'title'       => 'TEST1',
            'amount'      => 100,
            'date'        => "2021-03-10",
            'attachment'  => null,
            'status'      => 0
        ]);

        $this->json('put', "api/expense-users/".$userExpenses->id, $userExpenses->getAttributes())
            ->assertStatus(200)
            ->assertExactJson(
                [
                    'data' => [
                        'id'              => $userExpenses['id'],
                        'user_id'         => 1,
                        'title'           => $userExpenses['title'],
                        'amount'          => $userExpenses['amount'],
                        'attachment'      => null,
                        'date'            => (string) $userExpenses['date'],
                        'status'          => $userExpenses['status'],
                        'created_at'      => (string)$userExpenses['created_at'],
                        'updated_at'      => (string)$userExpenses['updated_at'],
                    ],
                    'status'=>200,
                    'success'=>true
                ]
            );
    }

}
