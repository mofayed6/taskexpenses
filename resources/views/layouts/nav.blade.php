
<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-user-circle"></i>
        <p>
            User Expenses
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{url('expense-users')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>All User Expenses </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{url('expense-users/create')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add</p>
            </a>
        </li>
    </ul>
</li>





