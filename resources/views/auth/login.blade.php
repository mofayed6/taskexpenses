<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    {{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <!--===============================================================================================-->
    <link href="{{ asset('admin/login/images/icons/favicon.ico') }}">
    <!--===============================================================================================-->
    <link href="{{ asset('admin/login/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--===============================================================================================-->
    <link href="{{ asset('admin/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
    <!--===============================================================================================-->
    <link href="{{ asset('admin/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}" rel="stylesheet">
    <!--===============================================================================================-->
    <link href="{{ asset('admin/login/vendor/animate/animate.css')}}" rel="stylesheet">
    <!--===============================================================================================-->
    <link href="{{ asset('admin/login/vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet">
    <!--===============================================================================================-->
    <link href="{{ asset('admin/login/vendor/select2/select2.min.css') }}" rel="stylesheet">
    <!--===============================================================================================-->
    <link href="{{ asset('admin/login/fonts/icomoon/style.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/login/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/login/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/login/css/style.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <main>
        <div class="d-lg-flex half">
            <div class="bg order-1 order-md-2" style="background-image: url({{asset('public/admin/login/images/img1.jpg')}})"></div>
            <div class="contents order-2 order-md-1">

                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7">
                            <h3>Login to <strong>Expenses APP</strong></h3>
                            <p class="mb-4">Join the largest corporate purchasing platform in the Middle East .</p>
                            <form method="Post" action="{{route('login')}}">
                                @csrf

                                <div class="form-group first">
                                    <label for="email">Username</label>
                                    <input hidden id="username" >

                                    <input id="email" type="text"  placeholder="Email" class="form-control email @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                           <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group last mb-3">
                                    <label for="password">Password</label>
                                    <input id="password" type="password"  placeholder="Password" class="form-control  @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="d-flex mb-5 align-items-center">
                                    <input class="checkbox" type="checkbox"  name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    {{ __('Remember Me') }}

                                    </label>
                                    <span class="ml-auto"><a href="#" class="forgot-pass">Forgot Password</a></span>
                                </div>

                                <button  class="btn btn-block btn-primary" type="submit" >
                                    {{ __('Login') }}
                                </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
</body>
</html>

