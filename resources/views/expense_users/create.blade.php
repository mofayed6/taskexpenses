@extends('layouts.layout')
@inject('model','App\Models\ExpenseUser')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <br>
    <br>
    @include('layouts.flash-message')
    <div class="box">
        <div class="box-body">

            {!! Form::model($model,[
              'action' => 'ExpenseUsersController@store',
              'enctype' => 'multipart/form-data',
            ]) !!}

            <div class="col">
                @include('expense_users/form')
                <div class="clearfix"></div>
                <div class="form-group">
                <br>
                <button class="btn btn-primary" type="submit">Save</button>
            </div>


            {!! Form::close() !!}
        </div>
        </div>
        </div>
    </div>



@endsection




