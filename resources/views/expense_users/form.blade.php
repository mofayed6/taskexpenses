<style>
    .image-upload>input {
        display: none;
    }
</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">بيانات عامة</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-10">
                            <label for="inputName">Title</label>
                            {!! Form::text('title',null,['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group col-md-10">
                            <label for="inputName">Amount</label>
                            {!! Form::number('amount',null,['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group col-md-10">
                            <label for="inputName">Attachment</label><br>
                            {!! Form::file('attachment')!!}
                        </div>
                        <div class="form-group col-md-10">
                            <label for="inputName">Date</label><br>
                            {!!  Form::date('date') !!}
                        </div>

</div>
</div>
<!-- /.card-body -->
</div>
<!-- /.card -->
</div>
</div>
</section>
<!-- /.content -->

