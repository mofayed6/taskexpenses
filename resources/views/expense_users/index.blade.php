@extends('layouts.layout')
@section('title')
    Expenses User
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/"><i class="fa fa-dashboard"></i>
                            </a></li>
                        <li class="breadcrumb-item active"> <a href="{{url('expense-users/create')}}" class="btn btn-success"><i class="fa fa-plus-circle"></i></a><li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <br>
    <br>
    <div class="box-body">
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                            <th>#</th>
                            <th>user</th>
                            <th>Expense</th>
                            <th>Amount</th>
                            <th>Attachment</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if(count($expenseUsers))
                        @foreach($expenseUsers as $expense)

                            <tr>
                                <td>{{$expense->id}}</td>
                                <td>{{$expense->user->name}}</td>
                                <td>{{$expense->title}}</td>
                                <td>{{$expense->amount}}</td>
                                <td><a href="{{ route('download',$expense->attachment) }}" target="_blank">{{ $expense->attachment }}</a></td>
                                <td>
                                    @if($expense->status == 0)
                                        <i class="btn btn-info">in progress</i>
                                    @elseif($expense->status == 1)
                                        <i class="btn btn-success">Approved</i>
                                    @else
                                        <i class="btn btn-danger">Rejected</i>
                                    @endif
                                </td>
                                <td>{{ $expense->date }}</td>
                                <td>
                                    <a href="{{url('expense-users/'.$expense->id)}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                    <a href="{{url('expense-users/'.$expense->id.'/edit')}}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                    <form action="{{ route('expense-users.destroy',$expense->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <div class="alert alert-warning alert-block">
                            <strong>NO Data</strong>
                        </div>
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>

@endsection

